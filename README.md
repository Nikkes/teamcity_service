# teamcity_service

Support optional build triggers in TeamCity service https://gitlab.com/gitlab-org/gitlab-ce/issues/13871
Protect TeamCity builds from triggering when a branch is deleted https://gitlab.com/gitlab-org/gitlab-ce/issues/17690